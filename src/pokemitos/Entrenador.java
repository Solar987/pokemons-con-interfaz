package pokemitos;
import java.sql.Connection;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.*;

/**
 *
 * @author jorge
 */
public class Entrenador implements Saveable{
    private String nombre;
    private String sexo;
    private int dinero;
    private int ID;
    private PreparedStatement psInsertar;
    private Statement stmnt;

    public Entrenador(String nombre, String sexo) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.dinero = 0;
        this.ID = 0;
    }

    public Entrenador(String nombre, String sexo, int dinero, int ID) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.dinero = dinero;
        this.ID = ID;
    }
    
    

    public int getID() {
        return ID;
    }

    public void setID(Connection con) {
        try {
        psInsertar= con.prepareStatement("INSERT INTO entrenador (nombre,dinero,sexo)"+
                " values(?,?,?)");
        psInsertar.setString(1, getNombre());
        psInsertar.setInt(2, getDinero());
        psInsertar.setString(3, getSexo());
        psInsertar.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error en la base de datos");
        }
        System.out.println("Segundo bloque del setID");
        try {
            String getID="SELECT ID FROM entrenador WHERE nombre='"+getNombre()+
                    "'";
            psInsertar= con.prepareStatement(getID);
            ResultSet result=psInsertar.executeQuery(getID);
            result.next();
            this.ID=Integer.parseInt(result.getString("ID"));
        } catch (SQLException ex) {
            System.out.println("Error en la base de datos");
        }
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getDinero() {
        return dinero;
    }

    public void setDinero(int dinero) {
        this.dinero = dinero;
    }

    @Override
    public boolean writeToDB(Connection con) throws SQLException {
        try {
            //stmnt = con.createStatement();
            String actualizar="UPDATE entrenador nombre = ?"
                    + "sexo = ?, dinero = ? WHERE id = ?";
            psInsertar= con.prepareStatement(actualizar);
            psInsertar.setString(1, getNombre());
            psInsertar.setString(2, getSexo());
            psInsertar.setInt(3, getDinero());
            psInsertar.setInt(4, getID());
            psInsertar.executeUpdate();
            return true;
        } catch (SQLException ex) {
        	System.out.println("Error al actualizar base de datos");
        }
        return false;
    }
    
}
