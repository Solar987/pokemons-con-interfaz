package pokemitos;
import com.google.gson.*;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.sql.*;
import java.time.LocalDate;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Controlador implements Initializable {
	//Variables que voy a usar para las instancias de las clases
	private Entrenador entrenador;
	private ArrayList<Pokemon> equipo;
	private Partida partidas;
	private Pokemon[] salvajes;
	
	// Panel 1
	@FXML Pane panel1;
	@FXML Button siguiente;
	@FXML Button si;
	@FXML Button no;
	@FXML Button salir;
	@FXML TextArea textohistoria;
	
	// Panel 2
	@FXML Pane panel2;
	@FXML Button jugar;
	@FXML Button cargar;
	
	// Panel 3
	@FXML Pane panel3;
	@FXML Button chico;
	@FXML Button chica;
	@FXML TextArea textogenero;
	
	// Panel 4 chico
	@FXML Pane panel4chico;
	@FXML Button listochico;
	
	// Panel 5 chica
	@FXML Pane panel5chica;
	@FXML Button listochica;

	// Panel partida
	@FXML Pane panelpartida;
	@FXML Button casa;
	@FXML Button combate;
	@FXML Button curar;
	@FXML Button tienda;

	// Panel casa
	@FXML Pane panelcasa;
	@FXML Button guardar;
	@FXML Button datos;
	@FXML Button volvercasa;
	
	// Panel tienda
	@FXML Pane paneltienda;
	@FXML Button volvertienda;
	
	// Mas de un panel
	@FXML TextField introducirnombre;
	@FXML DatePicker introducirfecha;
	@FXML TextField nombre;
	@FXML TextField fecha;

	// Image
	@FXML ImageView digimon;
	@FXML ImageView bailongo;
	@FXML ImageView willow;
	
	// String mensajes y cosas varias
	String[] mensajebienvenida = {"Encantado de conocerte..","Mi nombre es Willow..","Me dedico a buscar pokemons..","¿Estas listo para comenzar la aventura?"};
	int contador = 0;
	
	int id;
	boolean sexopersonaje;
	String nombrejugador;
	LocalDate fechajugador;
	int pokecuartos;
	
	String consulta;
	
	// Musica
	
	//Musica Windows
	// Cancion Intro Nier
	Media media = new Media("file:///D://Desktop//Clases//Interfaz//Musica//MusicaIntro1.mp3");
	MediaPlayer player = new MediaPlayer(media); 
	
	// Cancion Digimon
	Media media1 = new Media("file:///D://Desktop//Clases//Interfaz//Musica//Digimon1.mp3");
	MediaPlayer player1 = new MediaPlayer(media1); 
	
	// Soy inevitable para cuando capture un pokemon
	Media media2 = new Media("file:///D://Desktop//Clases//Interfaz//Musica//SoyInevitable.mp3");
	MediaPlayer player2 = new MediaPlayer(media2); 
	
	// Musica Tapion partida
	Media media3 = new Media("file:///D://Desktop//Clases//Interfaz//Musica//MusicaPartida.mp3");
	MediaPlayer player3 = new MediaPlayer(media3); 
	
	// Musica Casa - Mark Knopfler
	Media media4 = new Media("file:///D://Desktop//Clases//Interfaz//Musica//Musicacasa.mp3");
	MediaPlayer player4 = new MediaPlayer(media4); 
    
	// Musica Tienda
	Media media5 = new Media("file:///D://Desktop//Clases//Interfaz//Musica//MusicaTienda.mp3");
	MediaPlayer player5 = new MediaPlayer(media5); 
	
	//Musica Ubuntu
	// Cancion Intro Nier
	//Media media = new Media("file:///MusicaIntro1.mp3");
	//MediaPlayer player = new MediaPlayer(media); 
		
	// Cancion Digimon
	//Media media1 = new Media("file:///Digimon1.mp3");
	//MediaPlayer player1 = new MediaPlayer(media1); 
		
	// Soy inevitable para cuando capture un pokemon
	//Media media2 = new Media("file:///SoyInevitable.mp3");
	//MediaPlayer player2 = new MediaPlayer(media2); 
	
	public void pasartexto(ActionEvent action) {
		// Mensajes bienvenida
		if(contador < 4) {
			if(!siguiente.isPressed()) {
				textohistoria.clear();
				textohistoria.appendText(mensajebienvenida[contador]);
				contador++;
			}
		}else {
			siguiente.setVisible(false);
			si.setVisible(true);
			no.setVisible(true);
		}

	}
	
	// Boton Si
	public void comenzaraventura(ActionEvent action) {
		// Comenzar la aventura
	    player.stop();
	    player1.setCycleCount(MediaPlayer.INDEFINITE);
	    player1.play();
	    player1.setVolume(0.05);
		if (!si.isPressed()) {
			try {
				TimeUnit.SECONDS.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			panel1.setVisible(false);
			panel2.setVisible(true);
			jugar.setVisible(true);
			cargar.setVisible(true);
			digimon.setVisible(true);
		}
	}
	
	// Boton Jugar
	public void jugar(ActionEvent action) {
		// Comenzar la partida
		if(!jugar.isPressed()) {
			panel2.setVisible(false);
			textogenero.setText("¿Eres chico o chica?");
			chico.setVisible(true);
			chica.setVisible(true);
			panel3.setVisible(true);
		}
	}
	
	// Boton Chico
	public void escojerchico(ActionEvent action) {
		if(!chico.isPressed()) {
			panel4chico.setVisible(true);
		}
		id = 1;
		sexopersonaje = true;
		pokecuartos = 100;
	}
	
	// Boton Chica
	public void escojerchica(ActionEvent action) {
		if(!chico.isPressed()) {
			panel5chica.setVisible(true);
		}
	}
	
	// Partida
	public void partida(ActionEvent action) {
		player1.stop();
		player4.stop();
		player5.stop();
		player3.setCycleCount(MediaPlayer.INDEFINITE);
	    player3.play();
	    player3.setVolume(0.05);
		if(!listochico.isPressed() || !listochica.isPressed() || !volvercasa.isPressed()) {
			panelcasa.setVisible(false);
			paneltienda.setVisible(false);
			panelpartida.setVisible(true);
			guardar.setVisible(true);
			combate.setVisible(true);
			curar.setVisible(true);
			tienda.setVisible(true);
		}
	}
	
	// Entrar en casa
	public void entrarencasa(ActionEvent action) {
		player3.stop();
		player4.setCycleCount(MediaPlayer.INDEFINITE);
	    player4.play();
	    player4.setVolume(0.15);
		if(!casa.isPressed()) {
			panelcasa.setVisible(true);
		}
	}
	
	// Entrar en la tienda
	public void entrarenlatienda(ActionEvent action) {
		player3.stop();
		player5.setCycleCount(MediaPlayer.INDEFINITE);
	    player5.play();
	    player5.setVolume(0.05);
		if(!tienda.isPressed()) {
			paneltienda.setVisible(true);
		}
	}
	
	// Guardar Partida
	public void guardarpartida(ActionEvent action){
		try {
			Conexiones connectionClass = new Conexiones();
			System.out.println("1");
			Connection conexion = connectionClass.conectar();
			System.out.println("2");
			
			nombrejugador = introducirnombre.getText();
			System.out.println("3");
			fechajugador = introducirfecha.getValue();
			System.out.println("4");
			consulta = "Insert into personaje(id,nombre,fecha,sexo,pokecuartos) values ("+ id + "," + "'" + nombrejugador + "',"
					+ "'" + fechajugador + "'," + sexopersonaje + "," + pokecuartos +" );";
			Statement query = conexion.createStatement();
			System.out.println("5");
		    Statement o = null;
		    System.out.println(consulta);
			System.out.println("6");
			o = conexion.prepareStatement(consulta);
	        System.out.println("7");
            o.execute(consulta);
            System.out.println("8");
	        
			query.close();
			conexion.close();
		}catch(Exception e) {
			e.toString();
		}
	}
	
	
	// A partir de aquí es salir + boton no
	public void saliraventura(ActionEvent action) {
		// Salir de la aventura
		willow.setVisible(false);
		bailongo.setVisible(true);
		player.stop();
		player2.setCycleCount(MediaPlayer.INDEFINITE);
		player2.play();
		player2.setVolume(0.20);
		if (!no.isPressed()) {
			textohistoria.setText("No pasa nada vuelve cuando quieras.");
			salir.setVisible(true);
			no.setVisible(false);
			si.setVisible(false);
		}
	}
	
	// Salir del todo
	public void saliraventurafinal(ActionEvent action) {
		// Salir del todo en la aventura
		if(!salir.isPressed()) {
			System.exit(0);
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// Que no salga nada inecesario al iniciar
		
		// Botones
		si.setVisible(false);
		no.setVisible(false);
		salir.setVisible(false);
		jugar.setVisible(false);
		cargar.setVisible(false);
		chico.setVisible(false);
		chica.setVisible(false);
		guardar.setVisible(false);
		combate.setVisible(false);
		curar.setVisible(false);
		tienda.setVisible(false);
		
		// Paneles
		panel2.setVisible(false);
		panel3.setVisible(false);
		panel4chico.setVisible(false);
		panel5chica.setVisible(false);
		panelpartida.setVisible(false);
		panelcasa.setVisible(false);
		paneltienda.setVisible(false);

		
		// Imagenes
		digimon.setVisible(false);
		bailongo.setVisible(false);

		// Musica inicio
	    player.play();
	    player.setCycleCount(MediaPlayer.INDEFINITE);
	    player.setVolume(0.05);
	    //Esto son cosas que necesito para obtener una conexion a la base de datos:JORGE
	    partidas=new Partida();

	}
	///Metodos hechos por mi para el tema del guardado y carga////
	
	public void crearPartida() {
		//Este metodo es para crear una partida. Hay que ver como coger el genero y nombre
		entrenador=new Entrenador("Jorge","chico");
		//Ahora el metodo siguiente hace un INSERT y obtiene la id que le da la base de datos para asignarselo
		entrenador.setID(partidas.getCon());
		
	}
	public void cargarPartida() {
		//Voy a volcar todos los jugadores de la base de datos en un arraylist
		ArrayList<Entrenador> eleccion=partidas.getPartidas();
		//Aquí deberiíamos abrir un desplegable o una tabla y elegir nuestra partida
		//Y con un lebel por ejemplo que escriba la id
		int opcion=0;
		entrenador = eleccion.get(opcion);//Aqui se devuelve el entrenador deseado
		//Y todos sus pokemons
		equipo=partidas.getEquipo(entrenador.getID());
	}
	public void leerJson() {
		FileReader reader=null;
		try {
			File f=new File("/home/jorge/Escritorio/PracticaPokemon/pokedex.json");
			reader = new FileReader(f);
			Gson gson= new Gson();
			JsonReader jsonReader = new JsonReader(reader);
			salvajes=gson.fromJson(jsonReader, Pokemon[].class);
		} catch (FileNotFoundException ex) {
			System.out.println("Erro al encontrar archivo");
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				System.out.println("Ha habido un error");
			}
		}

	}
	
	//Metodo para devolver un pokemon aloeatorio del array de json
	public Pokemon salvajeAleatorio() {
        int aleatorio=(int)(Math.random()*11+1);
        Pokemon temp=salvajes[aleatorio];
        temp.actualizarStats();
        return temp;
	}
	public void capturarPokemon() {
		Pokemon oponente=salvajeAleatorio();
		oponente.setIdEntrenador(entrenador.getID());
		equipo.add(oponente);
	}
	public void guardarPartida() {
		try {
			entrenador.writeToDB(partidas.getCon());
			for(Pokemon p: equipo){
				p.writeToDB(partidas.getCon());
			}
		} catch (SQLException ex) {
			System.out.println("Ha habido un error");
		}

	}

}