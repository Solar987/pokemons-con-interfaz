package pokemitos;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge
 */
public class Partida {
    ArrayList<Entrenador> partidas;
    ArrayList<Pokemon> equipo;
    Connection con;
    public Partida(){
        try {
        	this.con=DriverManager.getConnection("jdbc:mysql://localhost/"+
        			"POKEMON?serverTimezone=UTC","jorge","iesVillaverde");
            String orden = "SELECT ID, nombre, sexo, dinero FROM entrenador";
            PreparedStatement preStatement= con.prepareStatement(orden);
            ResultSet rs = preStatement.executeQuery(orden);
            while(rs.next()){
                String nombre= rs.getString("nombre");
                String sexo= rs.getString("sexo");
                int dinero = Integer.parseInt(rs.getString("dinero"));
                int id = Integer.parseInt(rs.getString("ID"));
                partidas.add(new Entrenador(nombre,sexo,dinero,id));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Entrenador> getPartidas() {
        return partidas;
    }

    public ArrayList<Pokemon> getEquipo(int idEntrenador) {
        try {
            String orden="SELECT salud, name, type, hp, attack, sAttack, defense"
                    + "sDefense, speed, level, codEntrenador WHERE "
                    + "codEntrenador = ?";
            PreparedStatement preStatement=con.prepareStatement(orden);
            preStatement.setInt(1, idEntrenador);
            ResultSet rs = preStatement.executeQuery(orden);
            while(rs.next()){
                int codEntrenador=Integer.parseInt(rs.getString("codEntrenador"));
                int salud=Integer.parseInt(rs.getString("salud"));
                String name = rs.getString("name");
                String type = rs.getString("type");
                int HP = Integer.parseInt(rs.getString("hp"));
                int Attack = Integer.parseInt(rs.getString(""));
                int sAttack = Integer.parseInt(rs.getString(""));
                int Defense = Integer.parseInt(rs.getString(""));
                int sDefense = Integer.parseInt(rs.getString(""));
                int Speed = Integer.parseInt(rs.getString(""));
                int level = Integer.parseInt(rs.getString(""));
                Pokemon temporal = new Pokemon(codEntrenador, name, type, HP,
                        Attack, sAttack, Defense, sDefense, Speed, level, salud);
                equipo.add(temporal);
            }
            return equipo;
        } catch (SQLException ex) {
            Logger.getLogger(Partida.class.getName()).log(Level.SEVERE, null, ex);
        }
        return equipo;
    }

	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
    
}
