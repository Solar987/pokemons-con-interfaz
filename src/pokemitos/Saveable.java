package pokemitos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Álvaro Lillo Igualada - @alilloig
 */
public interface Saveable {
  //ResultSet readFromDB(Connection con) throws SQLException;
  boolean writeToDB(Connection con)throws SQLException;
}
