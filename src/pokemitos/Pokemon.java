package pokemitos;
import java.sql.*;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author jorge
 */
public class Pokemon implements Saveable {
	private int idEntrenador;
	private int salud;
	private String name;
	private String type;
	private int HP;
	private int Attack;
	private int sAttack;
	private int Defense;
	private int sDefense;
	private int Speed;
	private int level;
	private PreparedStatement psInsertar;
	private Statement stmnt;

	public Pokemon(int idEntrenador, String name, String type, int HP,
			int Attack, int sAttack, int Defense, int sDefense, int Speed,
			int level, int salud) {
		this.idEntrenador = idEntrenador;
		this.name = name;
		this.type = type;
		this.HP = HP;
		this.Attack = Attack;
		this.sAttack = sAttack;
		this.Defense = Defense;
		this.sDefense = sDefense;
		this.Speed = Speed;
		this.level = level;
		this.salud=salud;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getIdEntrenador() {
		return idEntrenador;
	}

	public void setIdEntrenador(int id) {
		this.idEntrenador = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getHP() {
		return HP;
	}

	public void setHP(int HP) {
		this.HP = HP;
	}

	public int getAttack() {
		return Attack;
	}

	public void setAttack(int Attack) {
		this.Attack = Attack;
	}

	public int getsAttack() {
		return sAttack;
	}

	public void setsAttack(int sAttack) {
		this.sAttack = sAttack;
	}

	public int getDefense() {
		return Defense;
	}

	public void setDefense(int Defense) {
		this.Defense = Defense;
	}

	public int getsDefense() {
		return sDefense;
	}

	public void setsDefense(int sDefense) {
		this.sDefense = sDefense;
	}

	public int getSpeed() {
		return Speed;
	}

	public void setSpeed(int Speed) {
		this.Speed = Speed;
	}

	@Override
	public String toString() {
		return "Pokemon{" + "id=" + idEntrenador + ", name=" + name +
				", type=" + type + ", HP=" + HP + ", Attack=" + Attack +
				", sAttack=" + sAttack + ", Defense=" + Defense + ", sDefense="
				+ sDefense + ", Speed=" + Speed + ", level=" + level + '}';
	}

	public void actualizarStats(){
		int aleatorio=(int)(Math.random()*11+1);
		this.level = aleatorio;
		//this.idEntrenador = id;
		this.name = name;
		this.type = type;
		this.HP = HP*level;
		this.Attack = Attack*level;
		this.sAttack = sAttack*level;
		this.Defense = Defense*level;
		this.sDefense = sDefense*level;
		this.Speed = Speed*level;

	}

	public int getSalud() {
		return salud;
	}

	public void setSalud(int salud) {
		this.salud = salud;
	}
	public void setLuchaVida(Pokemon oponente){
		int ataqueRival=(int) (oponente.getAttack()-(this.Attack*0.5));
		this.salud=this.salud-ataqueRival;
		if(this.salud<=0){
			this.salud=0;
		}
	}


	@Override
	public boolean writeToDB(Connection con) throws SQLException {
		String actualizar="UPDATE pokemon SET salud=?, name = ?, type=?, hp=?,"
				+ "attack=?, sAttack=?, defense=?, sDefense=?, speed=?, level=?,"
				+ "codEntrenador = ? ";

		try {
			psInsertar= con.prepareStatement(actualizar);
			psInsertar.setInt(1, getSalud());
			psInsertar.setString(2, getName());
			psInsertar.setString(3, getType());
			psInsertar.setInt(4, getHP());
			psInsertar.setInt(5, getAttack());
			psInsertar.setInt(6, getsAttack());
			psInsertar.setInt(7, getDefense());
			psInsertar.setInt(8, getsDefense());
			psInsertar.setInt(9, getSpeed());
			psInsertar.setInt(10, getLevel());
			psInsertar.setInt(11, getIdEntrenador());



			psInsertar.executeUpdate();
			return true;
		} catch (SQLException ex) {
            System.out.println("Error en la base de datos");
		}
		return false;
	}

}
